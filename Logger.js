module.exports.ERRORS = 0;
module.exports.IMPORTANT_INFO = 1;
module.exports.SOME_DETAILS = 2;
module.exports.ALL_DETAILS = 3;

module.exports.log = function (s, level) {
    var currentLevel = parseInt(process.env.REAL_ESTATE_LOGGER_LEVEL || '1');
    if (currentLevel >= (level || 1)) {
        console.log("#RE: " + (typeof(s) === 'string' ? s : JSON.stringify(s, null, 4)));
    }
};

// TODO: Implement email broadcasting in case of an error.
module.exports.error = function (s) {
     console.log("*********\n#RE ERROR: " + s + "\n*********");
};
