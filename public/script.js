// Code goes here
'use strict';

/*
function initialize() {
  var mapOptions = {
    zoom: 10,
    center: new google.maps.LatLng(43.6639409, -79.4634751)
  };

  var map = new google.maps.Map(
      document.getElementById('map-canvas'),
      mapOptions);
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
      '&signed_in=true&callback=initialize';
  document.body.appendChild(script);
}

window.onload = loadScript;
*/
angular.module('myApp', [ 'uiGmapgoogle-maps'  ])
// .config(function(uiGmapGoogleMapApiProvider) {
//     uiGmapGoogleMapApiProvider.configure({
//         //    key: 'your api key',
//         v: '3.17',
//         libraries: 'weather,geometry,visualization'
//     });
// })
.controller('REMapController', ['$scope', '$http', /*'uiGmapGoogleMapApi',*/
  function($scope, $http /*, uiGmapGoogleMapApi*/ ) {
    $scope.greetMe = 'World';
    $scope.map = {
      center: {
        latitude: 43.6639409,
        longitude: -79.4634751
      },
      zoom: 10,
      eventHandlers: {
            "dragend": handleMapMoves,
            "zoom_changed": handleMapMoves,
            "click": handleMapClick
      }
    };
    $scope.rectangles = [];
      
    function handleMapClick(maps, eventName, args) {
        console.log(eventName);
//        console.log(maps);
        console.log(JSON.stringify(args, null, 4));
        var e = args[0];
        var lat = e.latLng.lat(),
        lon = e.latLng.lng();
        console.log(lat + ", " + lon);
        
        $http.get("api/stat/lat/" + lat + "/lon/" + lon)
            .then(function(res) {
                $scope.greetMe = res.data.data;
            });
    }
    function handleMapMoves(map, eventName) {
        console.log(eventName);
        
        var bounds = map.getBounds();

        var ne = bounds.getNorthEast();
        var sw = bounds.getSouthWest();

        $http.get("api/zones/w/" + sw.lng() + "/e/" + ne.lng() + "/s/" + sw.lat() + "/n/" + ne.lat())
            .then(function(res) {
                // Clean up the old rectangles:
                for (var i in $scope.rectangles) {
                    var rect = $scope.rectangles[i];
                    rect.setMap(null);
                }
                $scope.rectangles = [];
            
                var zones = res.data.data;
                //console.log(JSON.stringify(zones, null, 4));
                for (var i in zones) {
                    var zone = zones[i].loc.coordinates[0];
/*                    var viewportPoints = [
//                        new google.maps.LatLng(zone.n, zone.e),
//                        new google.maps.LatLng(zone.s, zone.e),
                        new google.maps.LatLng(parseFloat(zone.s), parseFloat(zone.w)),
//                        new google.maps.LatLng(zone.n, zone.w),
                        new google.maps.LatLng(parseFloat(zone.n), parseFloat(zone.e))
                    ];
                    console.log(zone);
                    var viewportBox = new google.maps.Polyline({
                        path: viewportPoints,
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 4 
                    });
  */                  
                    //hack - TODO: fix the W > E issue in the dispatcher
                    /*if (zone.w > zone.e) {
                        zone.w ^= zone.e;
                        zone.e ^= zone.w;
                        zone.w ^= zone.e;
                    }*/
                    $scope.rectangles.push(new google.maps.Rectangle({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0, //0.35,
                        map: map,
                        bounds: new google.maps.LatLngBounds(
                            new google.maps.LatLng(zone[0][1], zone[0][0]),
                            new google.maps.LatLng(zone[2][1], zone[2][0]))
                    }));
                    console.log(zone);
                    
                    //viewportBox.setMap(map);
                }
            });

    }
    // uiGmapGoogleMapApi.then(function(maps) {
    //   console.log("OK")
    // });
  }
]);