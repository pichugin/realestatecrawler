var http = require('http');
var queryString = require("querystring");
var realEstateDispatcherFactory = require("./RealEstateDispatcher");
var zoneIterator = require("./ZoneIterator");
var logger = require('./Logger');

var realEstateDispatcher = realEstateDispatcherFactory();

var options = {
	host: 'www.realtor.ca',
	path: '/api/Listing.svc/PropertySearch_Post',
	headers: {
		"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91 Safari/537.36",
		"Accept": '*/*',
		"Content-Type": 'application/x-www-form-urlencoded'
	},
	port: '80',
	method: 'POST'
};

function log(message, level) {
    logger.log("app." + message, level);
}

var callback = function (zone, response) {
	var str = '';
	response.on('data', function (chunk) {
		str += chunk;
	});
	response.on('end', function () {
		handleResponse(zone, JSON.parse(str));
	});
};

function handleResponse(zone, data) {
	//logger.log(data);
	//realEstateDispatcher.dispatch(data.Results);
    zoneIterator.iterate(zone, data.Paging, data.Results,
            function acceptResult(results, zone) {
                realEstateDispatcher.dispatch(results, zone);
            },
            function nextRequest(zone) {
                doRequest(mlsRequestData, zone);
            });
}
        
var initialZone = {
    n: 43.723060,  // LatitudeMax
    s: 43.620109,  // LatitudeMin
    w: -79.429801, // LongitudeMin
    e: -79.360012  // LongitudeMax
};

var mlsRequestData = {
	"CultureId": 1,
	"ApplicationId": 1,
	"RecordsPerPage": 9,
	"MaximumResults": 9,
	"PropertyTypeId": 300,  // Single Family
	"PropertyTypeGroup": 1, // Residential
	"TransactionTypeId": 2,
	"SortOrder": "A",
	"SortBy": 1,
//	"LongitudeMin": initialZone.w, //"-79.429801",  //"-80.04913330078125",
//	"LongitudeMax": initialZone.e, //"-79.360012", //"-78.85711669921875",
//	"LatitudeMin":  initialZone.s, //"43.620109", //"43.47783585578777",
//	"LatitudeMax":  initialZone.n, //"43.723060", //"43.67978987796577",
	"PriceMin": 50000,
	"PriceMax": 0,
	"viewState": "m",
//	"CurrentPage": 1
};

var requestQueue = [];

function doRequest(requestData, zone) {
	requestData.LongitudeMin = zone.w;
	requestData.LongitudeMax = zone.e;
	requestData.LatitudeMin  = zone.s;
	requestData.LatitudeMax  = zone.n;
    requestData.CurrentPage  = zone.page || 1;

    requestQueue.push(function executeScheduledRequest() {
        var req = http.request(options, function(response) { 
            callback(zone, response); 
        });
        log("executeScheduledRequest(): Sending: " + JSON.stringify(requestData), logger.SOME_DETAILS);
        req.write(queryString.stringify(requestData));
        req.end();
    });
    log("doRequest(): Request has been put in the queue. The queue size: " + requestQueue.length,
            logger.SOME_DETAILS);
}

doRequest(mlsRequestData, initialZone);

setInterval(function traverseQueue() {
    log("traverseQueue(): Checking the queue... size: " + requestQueue.length, logger.SOME_DETAILS);
    var scheduledRequest = requestQueue.shift();
    if (scheduledRequest) {
        scheduledRequest();
    }
}, 1000);

/*
CultureId:1
ApplicationId:1
RecordsPerPage:9
MaximumResults:9
PropertyTypeId:300
TransactionTypeId:2
SortOrder:A
SortBy:1
LongitudeMin:-79.99964333143184
LongitudeMax:-78.80762672986934
LatitudeMin:43.58145920836657
LatitudeMax:43.783065920401015
PriceMin:50000
PriceMax:0
BuildingTypeId:17
BedRange:0-0
BathRange:0-0
ParkingSpaceRange:0-0
viewState:m
CurrentPage:1
*/
/*
CultureId:1
ApplicationId:1
RecordsPerPage:9
MaximumResults:9
PropertyTypeId:300
TransactionTypeId:2
SortOrder:A
SortBy:1
LongitudeMin:-79.99964333143184
LongitudeMax:-78.80762672986934
LatitudeMin:43.58145920836657
LatitudeMax:43.783065920401015
PriceMin:50000
PriceMax:0
BuildingTypeId:1
BedRange:0-0
BathRange:0-0
ParkingSpaceRange:0-0
viewState:m
CurrentPage:1
*/
