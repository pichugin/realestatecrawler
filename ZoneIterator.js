var logger = require('./Logger');

/*
zone: {                // INCOMING PARAMETER (example)
    latMin: 11.00
    latMax: 14.00
    lonMin: -19.00
    lonMax: -17.00
}

pageInfo: {            // INCOMING PARAMETER (example)
    "RecordsPerPage": 9,
    "CurrentPage": 1,
    "TotalRecords": 2953,
    "MaxRecords": 1500,
    "TotalPages": 167,
    "RecordsShowing": 1500,
    "Pins": 16
}

zoneCorrection: {      // OUTGOING PARAMETER (example)
    latMin: 11.00
    latMax: 12.50
    lonMin: -19.00
    lonMax: -18.00
    page: 3
}

Callback instructions to caller:
- save result (when the last page of the last sub-zone is retrieved)
- save result and request next page (when we don't need to devide the zone on smaller pieces)
- save result and request another zone (when the last page of the current sub-zone is retrieved, but there are more sub-zones)
- ignore result and request another zone (when the zone is too big and we need to divide it)

**/
function iterate(zone, pageInfo, data, acceptResultCb, nextRequestCb) {
    logger.log("ZoneIterator.iterate()", logger.SOME_DETAILS);
    logger.log("ZoneIterator.iterate(): " + JSON.stringify(pageInfo, null, 4), logger.ALL_DETAILS);
    var recordsPerPage = pageInfo.RecordsPerPage;
    var currentPage    = pageInfo.CurrentPage;
    var totalPages     = pageInfo.TotalPages;
    var totalRecords   = pageInfo.TotalRecords; // Total records in this zone
    var maxRecords     = pageInfo.MaxRecords;   // Total records we could get if iterated through 
                                                // all pages. We need this to be less or equal to TotalRecords.
    var recordsShowing = pageInfo.RecordsShowing; // Can't see the difference between MaxRecords. Weird...

    if (totalRecords <= maxRecords) {
        acceptResultCb(data, zone);
        // Now we just need to go through all the pages:
        if (currentPage <= totalPages) {
            var zoneCorrection = copyZone(zone);
            zoneCorrection.page = currentPage + 1;
            nextRequestCb(zoneCorrection);
        }
    } else {
        var verticalDiff = zone.n - zone.s;
        var horizontalDiff = zone.e - zone.w;
        var subzone1 = copyZone(zone);
        var subzone2 = copyZone(zone);

        if (verticalDiff > horizontalDiff) {
            var verticalMiddle = zone.s + verticalDiff / 2;
            subzone1.s = subzone2.n = verticalMiddle;
        } else {
            var horizontalMiddle = zone.w + horizontalDiff / 2;
            subzone1.e = subzone2.w = horizontalMiddle;
        }
        nextRequestCb(subzone1);
        nextRequestCb(subzone2);
    }
}

function copyZone(zone) {
    return  { n: zone.n, s: zone.s, w: zone.w, e: zone.e };
}

module.exports.iterate = iterate;
