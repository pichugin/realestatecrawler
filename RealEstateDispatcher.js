var dao = require('./RealEstateDao').getInstance();
var logger = require('./Logger');

//var STEP = 0.1; //TODO: Find out how many kilometers this is, and how big is the zone we need.

function RealEstateDispatcher() {
    this.stats = {};
}

function log(message, level) {
    logger.log("RealEstateDispatcher." + message, level);
}

function logDispatch(realEstatePropertyArray, zone) {
    log("logDispatch(): zone: Width:" + (zone.e - zone.w) + " Height:" + (zone.n - zone.s) + 
            " Properties:" + realEstatePropertyArray.length, logger.SOME_DETAILS);
}

RealEstateDispatcher.prototype.dispatch = function (realEstatePropertyArray, zone) {
    logDispatch(realEstatePropertyArray, zone);
    if (realEstatePropertyArray.length <= 0) {
        return;
    }
    var currentDate = getCurrentDateAsYYYYMMDD();
    var subTotals = {};
    var subTotalsKeys = [];
    var propertyType = realEstatePropertyArray[0].Building.Type;
	for (var i in realEstatePropertyArray) {
		var reProperty = realEstatePropertyArray[i];
        var key = reProperty.Building.Type; //TODO: add bedrooms, size, etc.
        var subTotal = subTotals[key];
        if (!subTotal) {
            subTotal = { subTotalPrice: 0, count: 0, type: reProperty.Building.Type };
            subTotals[key] = subTotal;
            subTotalsKeys.push(key);
        }
		subTotal.subTotalPrice += priceStringToNumber(reProperty.Property.Price);
        subTotal.count++;
        
        log("dispatch(): type: " + 
                reProperty.Building.Type + ", " +
                reProperty.Property.Type + "(" + reProperty.Property.TypeId + ")",
                logger.ALL_DETAILS);
    }
    for (var i in subTotalsKeys) {
        var subTotal = subTotals[subTotalsKeys[i]];
        var rePropertyToSave = {
            date: currentDate,
            loc: convertZoneToLocation(zone), /* getZoneCoords(
                reProperty.Property.Address.Longitude,
                reProperty.Property.Address.Latitude) */
            type: subTotal.type
        };
        dao.findOneByDateAndLocation(
                currentDate, 
                convertZoneToLocation(zone), 
                propertyType, 
                /* TODO: bedrooms, etc */ 
                (function (rePropertyToSave, subTotal) {
                    return function (oldData) {
                        if (oldData) {
                            oldData.priceAve = (oldData.count * oldData.priceAve + subTotal.subTotalPrice) / 
                                    (oldData.count + subTotal.count);
                            oldData.count += subTotal.count;
                            rePropertyToSave = oldData;
                        } else {
                            rePropertyToSave.count = subTotal.count;
                            rePropertyToSave.priceAve = subTotal.subTotalPrice / rePropertyToSave.count;
                        }
                        if (!rePropertyToSave.priceAve) {
                            logger.error("rePropertyToSave: " + JSON.stringify(rePropertyToSave, null, 4) + " oldData: " + JSON.stringify(oldData, null, 4) + " priceAve components: " + subTotal.subTotalPrice + " / " + rePropertyToSave.count);
                            throw new Error("priceAve is undefined!!!");
                        }
                        dao.save(rePropertyToSave);
                    };
                })(rePropertyToSave, subTotal)
        );
    }
};

/*
function getZoneCoords(lon, lat) {
    return {
        lon: Math.floor(lon / STEP) * STEP,
        lat: Math.floor(lat / STEP) * STEP,
        step: STEP
    };
}
*/
    
function getCurrentDateAsYYYYMMDD() {
    return new Date().toISOString().substring(0, 10).replace(/-/g, "");
}

function priceStringToNumber(price) {
    //logger.log(price + " --> " + price.replace(/[\$,]/g, ""), logger.ALL_DETAILS);
    return parseInt(price.replace(/[\$,]/g, ""));
}

function convertZoneToLocation(zone) {
    return {
        type: "Polygon",
        coordinates: [[ 
            [zone.w, zone.s], 
            [zone.e, zone.s],
            [zone.e, zone.n],
            [zone.w, zone.n],
            [zone.w, zone.s] 
        ]]
    };
}

function convertLocationToZone(loc) {
    return {
        s: loc.coordinates[0][0][0],
        n: loc.coordinates[0][2][0],
        w: loc.coordinates[0][0][1],
        e: loc.coordinates[0][1][1]
    };
}

module.exports = function() { return new RealEstateDispatcher(); };

/*
    Location
        -> Date
            -> Type
                -> Bedroom
                
// Storage schema:
// Perhaps, we'll have to store the same data in multiple formats for fast access.
{
	zone: {
        lon : -12.3,
        lat : 45.6,
        step : 0.1
    },
	history: {
        "2013-06-23": {
			byType : [
				{ 
					"1" : {
						"count" : 123,
						"priceAverage" : "123,456",
						"priceMedian"  : "100,000"
					}
				}
			]
		}
	}
}

// Plain format:
{
    date: 20130623,
    zone: {
        lon : -12.3,
        lat : 45.6,
        step : 0.1
    },
    type: 300,
    bedrooms: 2,
    priceAve: 123456,
    priceMedian: 100000,
    count: 123
}

// Criteria setup:
{
	"items" : [
		{ "label" : "House", "items" : [
				{ "label" : "ALL" },
				{ "label" : "Detached", "items" : [
						{ "label" : "ALL" },
						{ "label" : "bd2" },
						{ "label" : "bd3" },
						{ "label" : "bd4" },
						{ "label" : "bd5" }
					]
				},
				{ "label" : "Semi-Detached", "items" : [
						{ "label" : "bd3" },
						{ "label" : "bd4" },
						{ "label" : "bd5" }
					]
				},
				{ "label" : "Attached", "items" : [
						{ "label" : "bd3" },
						{ "label" : "bd4" }
					]
				},
				{ "label" : "Link" }
			]
		},
		{ "label" : "Townhouse", "items" : [
				{ "label" : "ALL" },
				{ "label" : "bd2" },
				{ "label" : "bd3" }
			]
		},
		{ "label" : "Apartment", "items" : [
				{ "label" : "ALL" },
				{ "label" : "bd2" },
				{ "label" : "bd3" },
				{ "label" : "bd4" }
			]
		}
	]
}

*/

/*
{
"Building": {
        "BathroomTotal": "2",
        "Bedrooms": "4",
        "SizeInterior": "1624 sqft",
        "StoriesTotal": "1.5",
        "Type": "House"
    },
...
}

{
    "Id": "15250067",
    "MlsNumber": "W3103598",
    "PublicRemarks": "Spacious Corner Unit With Beautiful Unobstructed Panoramic View. Profesionally Managed, Family Oriented Building Near Transportation, Schools, Shopping, Etc. Large Principle Rooms, Ensuite Laundry. Maintenance Fee Includes Taxes And Utilities. **** EXTRAS **** Fridge, Stove, Light Fixtures And Window Coverings Belonging To Owner. (Excl.Window Coverings And Washer Belonging To Tenant)",
    "Building": {
        "BathroomTotal": "1",
        "Bedrooms": "2",
        "Type": "Apartment"
    },
    "Property": {
        "Price": "$139,900",
        "Type": "Single Family",
        "Address": {
            "AddressText": "#722 - 3460 KEELE ST|Toronto, Ontario M3J1L9",
            "Longitude": "-79.4885178",
            "Latitude": "43.7511482"
        },
        "TypeId": "300",
        "OwnershipType": "Shares in Co-operative"
    },
    "Business": {},
    "Land": {},
    "PostalCode": "M3J1L9"
}
*/