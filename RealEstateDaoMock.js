var logger = require('./Logger');


function RealEstateDao() {
	
    var memoryCacheMap = {}; // { "date-w-e-s-n-type": property }
    
	logger.log('Database is being mocked ;)');
	
	this.save = function(reProperty, cb) {
        var p = reProperty;
        var key = p.date + '-' + p.loc.w + '-' + p.loc.e + '-' + p.loc.s + '-' + p.loc.n + p.type;
        logger.log("Mocking save: storing under the key = '" + key + "'");
        memoryCacheMap[key] = reProperty;
        if (typeof(cb) === 'function') {
            cb(reProperty);
        }
	};
	
	this.delete = function (id, cb) {
        cb();
	};

	this.getOne = function (id, cb) {
		if (!id) {
			cb(null);
		} else {
            cb({}); //TODO
		}
	};

    this.findOneByDateAndLocation = function (date, zone, type, cb) {
        var key = date + '-' + zone.w + '-' + zone.e + '-' + zone.s + '-' + zone.n + type;
        cb(memoryCacheMap[key]);
    };

	this.find = function (criteria, cb) {
        cb([]); //TODO
	};
	
}

module.exports.getInstance = function() { return new RealEstateDao(); };