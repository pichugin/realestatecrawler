var dao = require('./RealEstateDao').getInstance();

var express = require('express');
var bodyParser = require('body-parser');
//var seeds = require('./routes/seeds');
//var articles = require('./routes/articles');
//var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

app.set('port', process.env.PORT || 3000);
//app.set('views', __dirname + '/views');
app.use(bodyParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) { res.render("index.html"); });
app.get('/api/stat/lat/:lat/lon/:lon', getStatistics);
app.get('/api/zones/w/:w/e/:e/s/:s/n/:n', getZonesInRange);
//app.post('/api/seeds/:seedId', seeds.saveSeed);
//app.delete('/api/seeds/:seedId', seeds.deleteSeed);

app.listen(app.get('port'), function(){
	console.log('Express server listening on port ' + app.get('port'));
});

function getStatistics(req, res) {
    res.send({data: "KUKU: " + req.params.lat + ", " + req.params.lon});
}

function getZonesInRange(req, res) {
    var zone = { w: req.params.w, e: req.params.e, s: req.params.s, n: req.params.n };
    dao.findZones(convertZoneToLocation(zone), getDateAsYYYYMMDD(new Date()), function (result) {
        res.send({data: result});
    });
}

function convertZoneToLocation(zone) {
    return {
        type: "Polygon",
        coordinates: [[ 
            [zone.w, zone.s], 
            [zone.e, zone.s],
            [zone.e, zone.n],
            [zone.w, zone.n],
            [zone.w, zone.s] 
        ]]
    };
}
                  
function getDateAsYYYYMMDD(date) {
    return date.toISOString().substring(0, 10).replace(/-/g, "");
}
