var logger = require('./Logger');

function log(message, level) {
    logger.log("RealEstateDao." + message, level);
}

function RealEstateDao() {
	
	var mongoose = require("mongoose");
	
	// Here we find a database to connect to, defaulting to localhost if we don't find one.  
	var uristring = process.env.MONGOLAB_URI || process.env.MONGO_REALESTATE_URI || 'mongodb://localhost/realEstate';
	var mongoOptions = { db: { safe: true }};
	
	// Makes connection asynchronously.  Mongoose will queue up database
	// operations and release them when the connection is complete.
	mongoose.connect(uristring, mongoOptions, function (err, res) {
		if (err) { 
			logger.error('ERROR connecting to: ' + uristring + '. ' + err);
		} else {
			log('connect(): Succeeded connected to: ' + uristring, logger.SOME_DETAILS);
		}
	});
	
    // Mongoose doesn't support multi-dimensional array, have to jump through hoops :(
    var Mixed = mongoose.Schema.Types.Mixed;
    
	// Schema definition:
	var rePlainObjectSchema = new mongoose.Schema({
        date: String,
        loc: {
            type: {type: String},
            // Mongoose doesn't support multi-dimensional array, have to jump through hoops :(
            coordinates: { type: Mixed, default: [] } //[[[ Number ]]]
        },
        type: {type: String},
        bd: Number,             // Bedbrooms
        priceAve: Number,       // Price Average
        priceMed: Number,       // Price Median
        count: Number             // Property Counter
	});
	
	// Compiles the schema into a model, opening (or creating, if nonexistent) the 'Article' collection in the MongoDB database
	this.REPropertyPlain = mongoose.model('REProperty', rePlainObjectSchema);
	
	var that = this;
	
	this.save = function(reProperty, cb) {
        log("save(): id:" + reProperty._id, logger.SOME_DETAILS);
		that.getOne(reProperty._id, function(rePropertyFromDb) {
			if (!rePropertyFromDb) {
                log("save(): INSERTING NEW", logger.SOME_DETAILS);
				rePropertyFromDb = new that.REPropertyPlain(reProperty);
				for (var field in reProperty) {
					if (reProperty.hasOwnProperty(field)) {
						rePropertyFromDb[field] = reProperty[field];
					}
				}
			}
            log("save(): about to persist: " + JSON.stringify(rePropertyFromDb, null, 4), logger.ALL_DETAILS);
			rePropertyFromDb.save(function (err) {
				if (err) { 
					logger.error("Failed to save: " + JSON.stringify(reProperty, null, 4) + "\n - " + err);
                    throw err; //TODO: Not sure whether to throw it, or just pass back...
				}
                log("save(): saved, id: " + rePropertyFromDb._id, logger.SOME_DETAILS);
				if (typeof cb === 'function') {
                    cb(err, rePropertyFromDb);
                }
			});
		});
	};
	
	this.delete = function (id, cb) {
        that.REPropertyPlain.findByIdAndRemove(id, function (err,offer) {
            cb();
		});
	};

	this.getOne = function (id, cb) {
		if (!id) {
			cb(null);
		} else {
            log("getOne(): id: " + id, logger.SOME_DETAILS);
			that.REPropertyPlain.findOne({ '_id' : id}).exec(function(err, result) { 
				if (err) { 
					logger.error("Could not find REProperty [" + id + "]: " + err);
				}
                log("getOne(): found with id: " + result ? result._id : null, logger.SOME_DETAILS);
				cb(err ? null : result);
			});
		}
	};
    
    this.findOneByDateAndLocation = function (date, loc, type, cb) {
        log("findOneByDateAndLocation(): date: " + date + ", type: " + type, logger.SOME_DETAILS);
        log("findOneByDateAndLocation(): loc: " + loc, logger.ALL_DETAILS);
        that.REPropertyPlain.findOne({ 
            date : date,
            loc: { 
                $geoIntersects : {
                    $geometry : loc //{type: "Polygon", coordinates: [[[1, 1], [1,2], [2,2], [2,1], [1,1]]]}
                }
            },
            type: type
        }).exec(function(err, result) { 
            if (err) { 
                logger.error("Failed trying to find REProperty {date:" + date + 
                        ", loc:" + JSON.stringify(loc) + ", type:" + type + "}: " + err);
            }
            log("findOneByDateAndLocation(): FOUND id: " + (result?result._id:null), logger.SOME_DETAILS);
            cb(err ? null : result);
        });
    };

	this.find = function (criteria, cb) {
		var query = that.REPropertyPlain.find({});
		query.where('lastId').gt(criteria.lastId);
		query.where('status').eq(criteria.status);
		query.limit(criteria.pageSize);
		query.exec(function(err, result) { 
			if (err) {
				logger.error("Failed fetching RE properties: " + err); 
			}
			cb(err ? null : result);
		});
	};
    
    
    this.findZones = function (loc, date, cb) {
        that.REPropertyPlain.find({ 
            'date' : date,  // For now let's just bring all zones for a specified date,
                            // but ideally we should find all zones ever stored.
            'loc': { 
                $geoIntersects : {
                    $geometry : loc
                }
            }
        }).exec(function(err, result) { 
            if (err) { 
                logger.error("Failed to find zones {date:" + date + 
                        ", loc:" + JSON.stringify(loc) + "}: " + err);
            }
            cb(err ? null : result);
        });
    };
}

module.exports.getInstance = function() { return new RealEstateDao(); };